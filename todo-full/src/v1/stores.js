import { writable } from 'svelte/store'

export const message = writable('')

message.subscribe( value => {
  if (!value) return
  const timeout = setTimeout( () => {
    message.set('')
    clearTimeout(timeout)
  }, 1000 )
})

// $: if ($message) {
//   const timeout = setTimeout( () => {
//     console.log('clear!!!')
//     $message = ''
//     clearTimeout(timeout)
//   }, 1000 )
// }


// let { set, subscribe } = writable('')

// export const message = {
//   set: (text) => {
//     console.log('set text!!!', text)
//     set(text)
//     if (text) {
//       const timeout = setTimeout( () => {
//         console.log('clear!!!')
//         set('')
//         clearTimeout(timeout)
//       }, 1000 )
//     }
//   },
//   subscribe
// }
