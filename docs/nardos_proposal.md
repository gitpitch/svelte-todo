React, Vue.js, Angular, Ember.js, Meteor, Mithril, Polymer, Aurelia, Backbone.js, Preact, Inferno... ¿sigo? La batalla de los frameworks de JavaScript parece no tener fin. Cada semana aparece "el nuevo framework que va a revolucionar el desarrollo de aplicaciones web".

Ahora parece tocarle el turno a Svelte, que tiene la particularidad de tener todo el aspecto de un framework pero que se resiste a ser tratado como tal.

Svelte pretende saldar el eterno debate entre "framework" y "librería" con un nuevo enfoque, en vez de traer un nuevo runtime que se ejecuta en nuestro explorador Svelte genera JavaScript minimalista y optimizado (plain vanilla js que le dicen), a partir de nuestro código.

Svelte sostiene que esto le permite no sólo generar aplicaciones super livianas sino también mejorar la experiencia del desarrollador "instrumentando" nuestro código.

En esta charla vamos a ver qué hay de cierto en todo esto, cuáles fueron las razones que llevaron a @Rich_Harris a crear Svelte, qué respuesta trae a los principales problemas que tenemos hoy para desarrollar aplicaciones, sus características principales y finalmente vamos a desarrollar interactivamente una aplicación para ver qué se siente usar Svelte.
