
Hello, my name is Sebastian Scarano

and I'd like to talk about

"Opening up modern web development for mere mortals with Svelte"

I'd like to talk abot Svelte, what it is, 

and how it provides a more approachable 

developer experience for people lacking a master's degree in

software engineering

There's a serious problem with today's web development,

and it's not a only a technological issue.

The industry seems to be trying really hard to convince us all 

that web development is no longer a thing for amateurs, 

but instead the exclusive kingdom of 10x developers.

And if we have a look at tools like 

React, Angular or even Vue, they seem to be right.

All this goes against the open spirit with which the whole web was 

conceived, when you could have a look at the source, 

and with just some HTML, CSS and JavaScript knowledge

you could start your journey in web development.

That's why I'm so exited to talk about this new tool called Svelte 

and it's radically new approach to building 

rich and complex user interfaces.

While traditional frameworks do most of their work in the users's 

browser, Svelte shifts that work to a compile step, 

and this approach not only gives us minimal and highly optimized 

 JavaScript code but, most important for us, provides a much more 

inclusive and inviting developer experience, where all the 

complexity of the platform is handled by the compiler.

it's really easy to get started and with just some 

HTML, CSS and JavaScript knowledeg anyone can start

developing their own web components

Somehow, it feels like getting back the old "anyone can do it" 

ethos of the early web days, applied to modern web 

development standars of today

(It's not a coincidence that Rich Harris, Svelte's creator, is not only a great software developer, but in fact, his main job is as a Graphics and Interactive Editor at the New York Times, and so he needs tools that allows him to interact with people with very different backgrounds.)

So in the presentation I'm going to talk about to give a short introduction to Svelte's philosophy, and I plan to interactively build and deploy a complete application to show on stage how it feels to develop web applications using svelte and how it allows more people to take their ideas to the web.

I'm a software Engineer from Buenos Aires, and I participate in several open source and open data communities in Argentina. I like  sharing my knowledge and working for an open and inclusive web. 

Thanks you very much and I really hope you like this proposal and considerer my application.