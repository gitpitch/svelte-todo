https://mozillafestival.org/proposals

[ Space ] openness, web literacy, digital inclusion

[ Format ] learning-forum, lab, hands-on

# What is the name of your session?

SvelteJs: a (non) framework for (non) web development experts

# What will happen in your session?

In recent years developing dynamic and compelling web applications got much harder. Mastering HTML, CSS and JavaScript will only get you started. The moment you pick a framework like React, Angular or Vue, you'll have to deal with state management, JSX, Typescript, hooks, functional programming, dependency injection, virtual DOMs, transpillers, module bundlers, or even more esoteric stuff like Redux or Vuex, and the list goes on...

Svelte, on the other hand, follows a radically new approach to building user interfaces.
Instead of delivering a shiny new framework that does the bulk of their work in the browser while the app is running, like traditional framework do, Svelte shifts that work into a compile step that happens only when you build your app, producing highly optimized plain vanilla JavaScript code.

The outcome of this approach is not only leaner and faster apps, but also a developer experience that is much more approachable for common people with no web-development degrees under his belt.

In fact, Svelte can hide away much of the complexity associated with traditional frameworks, allowing you to, once again, do a hell lot of useful stuff with just HTML, CSS and JavaScript.

After a quick introduction, participants will get a first-hand experience of building a basic web application with Svelte, and see how far they can go with just some basic concepts.

During the session participants will:

- Get a general introduction to modern web development and understand the problems that traditional framework aim to solve.
- Find out about Svelte new approach.
- Create a basic web application using one of Svelte's template.
- Base syntax and core features of Svelte
- Access a JSON web service to get data.
- React to user actions to filter data.
- Create components to reuse common functionality.
- Deploy the application using free alternatives like now, surge, or GitHub/GitLab pages.

# What is the goal or outcome of your session?

Participants will give their first steps with modern web development with the aid of a (non) framework that helps them get started.

# After the festival, how will you and your participants take the learning and activities forward?

This is just a starting point. Participants are expected to take advantage of the session to put their own ideas into practice and to get involved with Svelte's community and web development in general.

# How will you deal with varying numbers of participants in your session? What if 30 participants attend? What if there are 3?

From my experience, this kind of hands-on activities gives better results working in pairs or small groups of people. So if there are 10 or more participants I would tell them to make groups of three people. If the session is not too crowded, with my assistance participants could tackle more difficult examples.
